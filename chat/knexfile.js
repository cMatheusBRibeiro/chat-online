// Update with your config settings.
require('dotenv').config()

module.exports = {
  development: {
    client: "sqlite3",
    connection: {
      filename: "./app/database/db.sqlite",
    },
    migrations: {
      directory: "./app/database/migrations",
    },
    useNullAsDefault: true,
  },
  test: {
    client: "sqlite3",
    connection: {
      filename: "./app/database/test.sqlite",
    },
    migrations: {
      directory: "./app/database/migrations",
    },
    useNullAsDefault: true,
  },

  staging: {
    client: "mysql",
    version: "8.0.20",
    connection: {
      host: process.env.MYSQL_HOSTNAME,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: "chat",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: "./app/database/migrations",
      tableName: "knex_migrations",
    },
  },

  production: {
    client: "mysql",
    version: "8.0.20",
    connection: {
      host: process.env.MYSQL_HOSTNAME,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: "chat",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: "./app/database/migrations",
      tableName: "knex_migrations",
    },
  },
};
