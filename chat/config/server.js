
require('dotenv').config()
const express = require("express");
const consign = require("consign");
const expressValidator = require("express-validator");
const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());

app.set("view engine", "ejs");
app.set("views", "./app/views");

app.use(express.static("./app/public"));

app.use(express.urlencoded({ extended: true }));

app.use(expressValidator());

consign().include("app/routes").then("app/controllers").into(app);

module.exports = app;
