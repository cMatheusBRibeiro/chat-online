exports.up = function (knex) {
  return knex.schema.createTable("chat", function (table) {
    table.string("id").primary();
    table.string("surname").notNullable();
    table.string("text").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("chat");
};
