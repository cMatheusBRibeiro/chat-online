const connection = require("../database/connection");
const generateUniqueId = require("../utils/generateUniqueId");

const nullApelidoMessage = "Nome ou apelido é obrigatório";
const sizeApelidoMessage =
  "Nome ou apelido deve conter entre 3 e 15 caracteres";

const validaChat = (dados) => {
  const { apelido } = dados;

  if (apelido == null || apelido?.length == 0) return nullApelidoMessage;

  if (apelido.length < 3 || apelido.length > 15) return sizeApelidoMessage;

  return true;
};

const addMessage = async (surname, text, conn = connection) => {
  if (surname && text) {
    const id = generateUniqueId();
    await conn("chat").insert({ id, surname, text });
    const message = await getMessageById(id);
    return message;
  }
};

const getMessageById = async (id, conn = connection) => {
  const message = await conn("chat").where("id", id).select("*").first();
  return message;
};

const getMessages = async (conn = connection) => {
  const messages = await conn("chat").select("*");
  return messages;
};

const getParticipants = async (conn = connection) => {
  const participants = await conn("chat").select("surname").distinct();
  return participants;
};
module.exports = {
  validaChat,
  nullApelidoMessage,
  sizeApelidoMessage,
  getMessages,
  addMessage,
  getMessageById,
  getParticipants,
};
