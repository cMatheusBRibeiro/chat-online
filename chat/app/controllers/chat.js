const chatService = require("../services/chat");

const iniciaChat = async (application, req, res) => {
  let dados = req.body;
  const { apelido, test, connection } = dados;

  if (chatService.validaChat(dados) != true) {
    req.assert("apelido", "Nome ou apelido é obrigatório").notEmpty();
    req
      .assert("apelido", "Nome ou apelido deve conter entre 3 e 15 caracteres")
      .len(3, 15);
    let erros = req.validationErrors();

    if (erros) {
      let data = {
        validacao: erros,
        apelido,
      };
      test ? res.json(data) : res.render("index", data);
      return;
    }
  }

  const messages = await chatService.getMessages(connection);
  const participants = await chatService.getParticipants(connection);

  application.get("io")?.emit("msgParaCliente", {
    apelido,
    mensagem: ` acabou de entrar no chat`,
  });

  const state = {
    dados,
    messages,
    participants,
  };

  test ? res.json(state) : res.render("chat", state);
};

module.exports = { iniciaChat };
