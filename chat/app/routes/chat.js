const chatRoutes = (router) => {
  router.get(
    "/chat",
    async (req, res) =>
      await router.app.controllers.chat.iniciaChat(router, req, res)
  );

  router.post(
    "/chat",
    async (req, res) =>
      await router.app.controllers.chat.iniciaChat(router, req, res)
  );
};

module.exports = chatRoutes;