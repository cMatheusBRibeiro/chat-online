const xlsToJson = require("xls-to-json")

console.log("Gerando casos de teste...")

xlsToJson({
        input: "../input.xlsx",
        output: `./input.json`,
        sheet: "input",
    },
    (err, result) => {
        if (err) throw err
    }
)

console.log("Casos de teste gerados com sucesso!!")