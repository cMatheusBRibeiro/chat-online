/// <reference types="cypress" />

// @ts-check
// @ts-ignore

import PageLogin from "./pages/PageLogin"
import PageChat from "./pages/PageChat"

// @ts-ignore
import testes from "../../input.json"

beforeEach(() => {
    cy.intercept("**/socket.io/?EIO=3&transport=polling&t=*").as("login")
    PageLogin.visitChat()
})

describe("Testes do chat", () => {
    testes.forEach((teste, index) => {
        if (teste['ID Teste']) {
            it(`${teste['ID Teste']} - ${teste["Descrição"]}`, () => {
                PageLogin.setUserName(teste['Nome'])
                PageLogin.clickBtEntrar(teste["Resultado Esperado"])

                PageChat.typeMessage(teste['Mensagem'])

            })
        }
    })

})