/// <reference types="cypress" />

// @ts-check
// @ts-ignore

class PageChat {
    constructor() {}

    typeMessage(message) {
        if (message !== "" && message !== "*****") {
            cy.get("#mensagem").type(message)
            this.sendMessage()
        }
    }

    sendMessage() {
        cy.get("#enviar_mensagem").click()
    }
}

export default new PageChat()