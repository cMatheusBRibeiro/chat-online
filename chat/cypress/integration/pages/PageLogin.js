/// <reference types="cypress" />

// @ts-check
// @ts-ignore



class PageLogin {
    constructor() {}

    visitChat() {
        // @ts-ignore
        cy.visit(process.env.SITE_URL)
    }

    setUserName(name) {
        cy.get("#apelido").type(name)
    }

    clickBtEntrar(mensagem) {
        cy.get("#entrar").click().then(() => {
            this.verifyError(mensagem)
        })
    }

    verifyError(mensagem) {
        if (mensagem.toUpperCase() !== "SUCESSO" && Cypress.$(".alert").is(":visible")) {
            cy.get(".alert").then(($el) => {
                if (!$el[0]["innerText"] === mensagem) throw Error($el[0]["innerText"])
                cy.end()
            })
        } else {
            cy.wait("@login")
        }
    }
}

export default new PageLogin()
