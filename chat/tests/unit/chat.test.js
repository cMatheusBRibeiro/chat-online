const { validaChat, nullApelidoMessage, sizeApelidoMessage} = require("../../app/services/chat");

test("Apelido Null", () => {
  const dados = {
    apelido: null,
  };
  expect(validaChat(dados)).toBe(nullApelidoMessage);
});

test("Apelido Vazio", () => {
  const dados = {
    apelido: "",
  };
  expect(validaChat(dados)).toBe(nullApelidoMessage);
});

test("Apelido Maior que 15 characteres", () => {
  const dados = {
    apelido: "1234567890123456",
  };
  expect(validaChat(dados)).toBe(sizeApelidoMessage);
});

test("Apelido Menor que 3 characteres", () => {
  const dados = {
    apelido: "12",
  };
  expect(validaChat(dados)).toBe(sizeApelidoMessage);
});

test("Apelido Ok", () => {
  const dados = {
    apelido: "Apelido",
  };
  expect(validaChat(dados)).toBe(true);
});
