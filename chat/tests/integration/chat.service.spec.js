const connection = require("../../app/database/connection");
const chatService = require("../../app/services/chat");
const generateUniqueId = require("../../app/utils/generateUniqueId");

const mockData = {
  id: generateUniqueId(),
  surname: "Jest da Silva",
  text: "Mensagem",
};

describe("chat-service", () => {
  beforeEach(async () => {
    await connection.migrate.rollback();
    await connection.migrate.latest();
    await connection("chat").insert(mockData);
  });

  afterAll(async () => {
    await connection.destroy();
  });

  it("[SERVICE] should be able to add a message", async () => {
    const message = await chatService.addMessage(
      mockData.surname,
      mockData.text,
      connection
    );
    expect(message).toHaveProperty("id");
    expect(message).toHaveProperty("surname");
    expect(message).toHaveProperty("text");
    expect(message?.id).toHaveLength(8);
  });

  it("[SERVICE] should be able to get a message by id", async () => {
    const message = await chatService.getMessageById(mockData.id, connection);

    expect(message).toHaveProperty("id");
    expect(message).toHaveProperty("surname");
    expect(message).toHaveProperty("text");
    expect(message?.id).toBe(mockData?.id);
    expect(message?.text).toBe(mockData?.text);
    expect(message?.surname).toBe(mockData?.surname);
  });

  it("[SERVICE] should be able to get messages", async () => {
    const messages = await chatService.getMessages(connection);
    expect(messages).toHaveLength(1);
    expect(messages[0]).toEqual(mockData);
  });

  it("[SERVICE] should be able to get participants", async () => {
    const messages = await chatService.getParticipants(connection);
    expect(messages).toHaveLength(1);
    expect(messages[0]).toEqual({ surname: mockData.surname });
  });
});
