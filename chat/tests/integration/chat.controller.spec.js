console.log = function () {};
console.info = function () {};
const server = require("../../config/server");
const connection = require("../../app/database/connection");
const request = require("supertest");
const { sizeApelidoMessage, nullApelidoMessage } = require("../../app/services/chat");

const mock = {
  apelido: "Carlos",
  test: true,
  connection,
};

describe("chat-controller", () => {
  beforeEach(async () => {
    await connection.migrate.rollback();
    await connection.migrate.latest();
  });

  afterAll(async () => {
    await connection.destroy();
  });

  it("[POST:[/chat]] should be able to receive error on apelido nullable", async () => {
    const response = await request(server).post("/chat").send({ test: true });
    expect(response.body).toHaveProperty("validacao");
    expect(response.body.validacao[0]).toHaveProperty("msg");
    expect(response.body.validacao[0].msg).toBe(nullApelidoMessage);
  });

  it("[POST:[/chat]] should be able to receive error on apelido length higher then 15", async () => {
    const response = await request(server)
      .post("/chat")
      .send({ apelido: "0123456789123456", test: true });
    expect(response.body).toHaveProperty("validacao");
    expect(response.body.validacao[0]).toHaveProperty("msg");
    expect(response.body.validacao[0].msg).toBe(sizeApelidoMessage);
  });

  it("[POST:[/chat]] should be able to receive error on apelido length less then 3", async () => {
    const response = await request(server)
      .post("/chat")
      .send({ apelido: "oi", test: true });
    expect(response.body).toHaveProperty("validacao");
    expect(response.body.validacao[0]).toHaveProperty("msg");
    expect(response.body.validacao[0].msg).toBe(sizeApelidoMessage);
  });

  it("[POST:[/chat]] should be able to enter on a chat room", async () => {
    const response = await request(server).post("/chat").send(mock);
    expect(response.body).toHaveProperty("dados");
    expect(response.body.dados).toHaveProperty("apelido");
    expect(response.body.dados.apelido).toBe(mock.apelido);
  });

  it("[GET:[/chat]] should be able to get a message by id", async () => {
    const response = await request(server).get("/chat").send(mock);
    expect(response.body).toHaveProperty("dados");
    expect(response.body.dados).toHaveProperty("apelido");
    expect(response.body.dados.apelido).toBe(mock.apelido);
  });
});
