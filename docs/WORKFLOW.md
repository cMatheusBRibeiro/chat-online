# FLUXO DE TRABALHO
Um fluxo de trabalho define as branches do processo de desenvolvimento, seus papeis e quais membros da equipe podem modificá-las e como podem fazê-lo. O fluxo definido para este projeto é definido em três branches - a primeira voltada para o desenvolvimento das funções da aplicação, a segunda voltada para o teste destas funções, e a terceira e última disponibiliza essas novas funções para o cliente.


## Develop
A branch de desenvolvimento recebe as alterações e implementações de novas funções na aplicação, servindo como ambiente de integração da equipe de desenvolvimento.
Desta forma, os desenvolvedores do projeto são os únicos que podem fazer commits e pull requests nesta branch. Dentro da pipeline são feitos testes nesta branch únicamente a fim de verificar se aplicação conitinuará executável com as alterações feitas, sem que haja comprometimento do fluxo de desenvolvimento.

## Homolog
Uma vez que as alterações são commitadas na branch de _Develop_ e passam os testes iniciais, estas alterações passam pra branch de homologação para que mais testes sejam feitos. Nesta branch a equipe de testers implementam uma série de testes unitários e testes de integração para verificar se o que foi feito atende aos níveis de qualidade exigidos, e testes de consistência executados via bot. Caso essas alterações passem por todos os testes feitos pela equipe, será feito o commit nesta branch pela própria equipe de testers, e as alterações ficam disponíveis para pull request tanto pelos testers como pela equipe de desenvolvimento.

## Master
A última etapa do fluxo de trabalho consiste na disponibilização das alterações supracitadas na branch principal da aplicação. Uma vez que estas sejam validadas, passando por todos os testes e sejam aprovadas pelo tech lead, as alterações são commitadas na _master_, ou seja, são disponibilizadas para o cliente. Além disso, esta branch aceitará pull requests do tech lead e das equipes de teste e de desenvolvimento.