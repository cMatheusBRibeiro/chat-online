# AWS EC2

O ambiente EC2 é utilizado para hospedar a aplicação. Por se tratar de uma instância de uma máquina virtual, o EC2 não obriga o uso de demais serviços nem exige alterações no código para que se adeque a um padrão - como cobrado pelo AWS Lambda, por exemplo, que é orientado a eventos. O EC2 permitiu o uso da aplicação utilizada como exemplo para este projeto sem maiores alterações, sendo necessário apenas a instalação das dependências para se executar a aplicação.


## O que faz

EC2 (Elastic Compute Cloud) é um serviço web de computação em nuvem, que entrega recursos computacionais sob demanda e focado em elasticidade, não precisando provisionar recursos em excesso para atender demandas extraordinárias, adaptandando os recursos fornecidos para atender as necessidades em tempo real.
Disponibiliza capacidade computacional redimensionável em nuvem com a principal vantagem de ser facilmente ajustado em questões de escalabilidade, oferecendo controle abrangente dos recursos computacionais.
