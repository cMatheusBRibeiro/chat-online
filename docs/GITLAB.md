# GITLAB

Escolhido como a plataforma de hospedagem do projeto por seu suporte nativo a funções DevOps, especificamente a pipeline que ajuda a compor a [integração contínua](./CI.md). É nele que fazemos as alterações do projeto e as disponibilizamos para que toda a equipe de desenvolvimento possa trabalhar de forma colaborativa e eficiente.


## O que é

Gitlab é uma plataforma de hospedagem de códigos-fonte que permite que a equipe de desenvolvimento contribua de forma colaborativa em projetos privados ou abertos com implementação de funções Git, como controle de versionamento e segmentação do projeto em diferentes branches.
Focando cada vez mais em ferramentas de DevOps, o Gitlab fornece ferramentas de integração e entrega contínua e de acompanhamento da qualidade do código, performance e testes de usabilidade.
