# INTEGRAÇÃO CONTÍNUA

Por se tratar de um processo ou abordagem do projeto, a aplicação de CI foi feita com o uso conjunto de um [fluxo de trabalho](./WORKFLOW.md) bem definido e a pipeline fornecida pelo [Gitlab](./GITLAB.md). Com isso, foi possível consolidar as alterações feitas pelos diferentes membros da equipe de forma concisa sem interferir no fluxo de desenvolvimento, submetendo estas alterações a uma série de testes e validando se estão aptas para publicação, oferecendo assim uma integração eficaz e contínua no projeto.


## O que é

A Integração Contínua (continuous integration - CI) é um processo de automação para os desenvolvedores, onde mudanças no código de uma aplicação são desenvolvidas, testadas e consolidadas regularmente em um repositório compartilhado. Se mostra a solução ideal para projetos que possuam desenvolvedores trabalhando ao mesmo tempo em diferentes recursos na mesma aplicação, ajudando a evitar conflitos entre as ramificações.
Permite que os desenvolvedores consolidem as alterações no código da aplicação em uma única ramificação compartilhada com facilidade e frequentemente. As mudanças são consolidadas e depois validadas através da criação automática da aplicação, passando por uma série de testes automatizados - geralmente de unidade e integração - feitos para garantir que as mudanças não prejudiquem o funcionamento da aplicação.
De forma geral, tudo é testado, incluindo classes, funções e diferentes módulos que compõem a aplicação. Em caso de conflito entre os códigos novos e existentes, a CI facilita a identificação e correção destes erros.
O uso da integração contínua não só aumenta a frequencia das distribuições de atualizações como aumenta a produtividade da equipe e melhora a qualidade geral do projeto, uma vez que envolve também a aplicação de uma rotina de testes automatizados em que a qualidade desta rotina impacta diretamente na quantidade de erros encontrados e a facilidade com a qual estes são corrigidos.