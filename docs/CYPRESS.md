# CYPRESS

O Cypress foi escolhido por ser uma ferramenta open-source com uma curva de aprendizado pequena, de fácil instalação e configuração. Os testes do front-end da aplicação são feitos através do Cypress, onde testamos o envio e recebimento das mensagens e sua exibição na página da aplicação.


## O que é

Cypress é um framework de testes automatizados end-to-end utilizando JavaScript que é executado no mesmo ciclo de execução da própria aplicação. Permite a criação, execução e debugging de testes de unidade, integração e de ponta a ponta, oferecendo também funções como a criação de snapshots conforme os testes são executados e a exibição de logs de erro e stack traces.
