# Ideias

## Faceis

- Criação de grupos de reuniões temporários, no qual assim que for finalizado, será gerado um relatório do que foi conversado durante e disponibilizado em uma aba de relatórios do servidor;
- Autenticação apenas para quem for interagir, observadores não precisam autenticar;
- Opção para autenticação obrigatória a todos ou apenas a quem for interagir.

## Medias

- Biblioteca de arquivo compartilhado;

## Dificeis

- Compartilhamento de mensagens para e-mails, whats e etc...;
- 

