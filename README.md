# DevOps Chat Online

Este repositório é utilizado para compartilhar as entregas para a disciplina de Laboratório de Projetos de Banco de Dados V, ministrada pelo Professor Eduardo Sakaue.

O objetivo do projeto é implementar um conjunto de melhores práticas de desenvolvimento de software (Dev) e operações de TI (Ops), DevOps para disponibilizar a aplicações de Chat Online.

## Índice

- [DevOps Chat Online](#devops-chat-online)
    - [Índice](#índice)
    - [O que é](#o-que-é)
    - [Equipe](#equipe)
    - [Tecnologias](#tecnologias)
    - [Entregas](#entregas)
        - [Entregas 1](#entrega-1-28mar2021)
        - [Entregas 2](#entrega-2-18abr2021)
        - [Entregas 3](#entrega-3-16mai2021)
        - [Entregas 4](#entrega-4-05jun2021)


## O que é

Projeto desenvolvido pelos alunos da FATEC - Professor Jessen Vidal, para a matéria Laboratório de Projeto de Banco de Dados, ministrada pelo Professor Eduardo Sakaue.

Este sistema tem o objetivo de criar um processo completo de DevOps para aplicações de Chat Online.


## Equipe

- **Product Owner** - Juliana Aline da Costa
- **Scrum Master** - Matheus Braga Ribeiro
- Arthur Braga Reis
- Felipe Nicoletti Reis Mário
- Gabriel Fernandes Giraud
- Lucas Barcelos
- Rodrigo César Reis


## Tecnologias

- [GITLAB](./docs/GITLAB.md)

- [Workflow](./docs/WORKFLOW.md)

- [Integração Contínua (CI)](./docs/CI.md)

- [CYPRESS](./docs/CYPRESS.md)

- [AWS EC2](./docs/AWS_EC2.md)

## Entregas

### Entrega 1 - 28/Mar/2021
- Definir qual o host iremos utilizar;
- Criar acesso de hospedagem;
- Definir qual plataforma iremos utilizar para os testes automatizados.

### Entrega 2 - 18/Abr/2021
- Definição do Fluxo de Trabalho;
- Definição do Método de CI;
- Implantação do Método de CI de backend/api;
- Criação dos testes para integração com o CI;
- Adiantamento da definição de método de CD;
- Documentação.

### Entrega 3 - 16/Mai/2021
- Definição do método de CD;
- Documentação do método de CD;
- Implementação do CI de banco de dados;
- Criação de bot para utilização nos testes de front-end.

### Entrega 4 - 05/Jun/2021

- Implementação do deploy;
- Separação da documentação das tecnologias em pastas;
- Implementação do bot na rotina de testes CI;
- Migração do bot para a branch de homologação;
- Criação de testes para integração com o deploy;
- Documentação sobre cada ferramenta, explicando o que é e o por que utilizamos essa ferramenta;
- Separação dos deploys de produção e homologação.

